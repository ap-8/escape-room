package com.kel8.escaperoom.events;

import com.kel8.escaperoom.service.ChatService;
import com.linecorp.bot.model.event.FollowEvent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@LineMessageHandler
public class NewFollowerEventHandler {

    @Autowired
    private ChatService chatService;

    @EventMapping
    public void newFollowerEvent(FollowEvent followEvent){
        String replytoken = followEvent.getReplyToken();
        List<Message> messageList = new ArrayList<>();
//        chatService.sendResponse(replytoken, messageList);
    }

}
