package com.kel8.escaperoom.events;

import com.kel8.escaperoom.engine.Player;
import com.kel8.escaperoom.service.ApiService;
import com.kel8.escaperoom.service.ChatService;
import com.kel8.escaperoom.service.GameService;
import com.kel8.escaperoom.templates.GameTemplate;
import com.kel8.escaperoom.templates.HelpTemplate;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@LineMessageHandler
public class TextEventHandler {
    @Autowired
    private ChatService chatService;

    @EventMapping
    public void textEvent(MessageEvent<TextMessageContent> messageEvent) throws IOException {
        String message = messageEvent.getMessage().getText().toUpperCase();
        String replyToken = messageEvent.getReplyToken();
        Source source = messageEvent.getSource();
        boolean playerAvailable = GameService.checkPlayer(source.getUserId());
        ApiService api = new ApiService();
        Player player;
        String msg;
        if ("/HELP".equals(message)) {
            chatService.sendResponse(replyToken, HelpTemplate.helpMessage);
        } else if ("/START".equals(message)) {
            if (!playerAvailable) {
                GameService.addPlayer(new Player(source.getUserId()));
                player = GameService.getPlayer(source.getUserId());
                msg = player.getQuestionAndAnswer();
                chatService.sendStringResponse(replyToken,msg);
            } else {
                chatService.sendStringResponse(replyToken, "Anda sudah memulai game");
            }
        } else if ("/STATUS".equals(message)) {
            if (playerAvailable) {
                player = GameService.getPlayer(source.getUserId());
                chatService.sendStringResponse(replyToken, GameTemplate.getStatus(player.healthPoint,player.correctAnswer));
            } else {
                chatService.sendStringResponse(replyToken, "Anda belum memulai game");
            }
        } else if ("/QUIT".equals(message)) {
            if (!playerAvailable) {
                chatService.sendStringResponse(replyToken, "Anda belum memulai game");
            } else {
                GameService.removePlayer(source.getUserId());
                chatService.sendStringResponse(replyToken, "Terimakasih telah memainkan escape room :)");
            }
        } else if ("A".equals(message)||"B".equals(message)||"C".equals(message)||"D".equals(message)) {
            boolean flag = false;
            boolean win = false;
            if (playerAvailable) {
                player = GameService.getPlayer(source.getUserId());
                List<Message> replyMessage = new ArrayList<>();
                if (message.equals(player.getAnswerFromRoom())) {
                    msg = "Jawaban anda benar";
                    player.correct();
                    if (player.roomNum == 11) {
                        GameService.removePlayer(source.getUserId());
                        win = true;
                    }
                } else {
                    msg = "Jawaban anda Salah";
                    player.wrong();
                    if (player.healthPoint == 0) {
                        GameService.removePlayer(source.getUserId());
                        flag = true;
                    }
                }
                replyMessage.add(new TextMessage(msg));
                if (!flag) {
                    replyMessage.add(new TextMessage(player.getQuestionAndAnswer()));
                } else {
                    replyMessage.add(new TextMessage("Nyawa anda telah habis :("));
                }
                if (win) {replyMessage.add(new TextMessage("Selamat anda telah memenangkan game\n Poin anda: " + player.correctAnswer));}
                chatService.sendResponse(replyToken,replyMessage);
            } else {
                chatService.sendStringResponse(replyToken,"Mulai permainan agar dapat menjawab soal");
            }
        } else if ("./GET".equals(message)) {
            chatService.sendStringResponse(replyToken, api.getQuestionAndChoices(3));
        } else {
            chatService.sendStringResponse(replyToken, "Perintah tidak diketahui");
        }
    }
}
