package com.kel8.escaperoom.events;

import com.kel8.escaperoom.service.ChatService;
import com.linecorp.bot.model.event.JoinEvent;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;

@LineMessageHandler
public class JoinEventHandler {

    @Autowired
    private ChatService chatService;

    @EventMapping
    public void joinGroupEvent(JoinEvent joinEvent) {
        chatService.sendStringResponse(joinEvent.getReplyToken(), "Halooo gengs!");
    }
}
