package com.kel8.escaperoom.engine.factory.room;

import com.kel8.escaperoom.service.ApiService;

import java.io.IOException;

public class Room5 implements Room{

    @Override
    public String getQuestionAndAnswer() throws IOException {
        return ApiService.getQuestionAndChoices(5);
    }

    @Override
    public String getAnswer() throws IOException {
        return ApiService.getAnswer(5);
    }

}
