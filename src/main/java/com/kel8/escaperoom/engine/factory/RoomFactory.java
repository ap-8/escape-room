package com.kel8.escaperoom.engine.factory;

import com.kel8.escaperoom.engine.factory.room.*;

public class RoomFactory {
    public static int availableRoom = 10;
    public Room getRoom(int roomNum) {
        if (roomNum == 1) {
            return new Room1();
        }
        else if (roomNum == 2) {
            return new Room2();
        }
        else if (roomNum == 3) {
            return new Room3();
        }
        else if (roomNum == 4) {
            return new Room4();
        }
        else if (roomNum == 5) {
            return new Room5();
        }
        else if (roomNum == 6) {
            return new Room6();
        }
        else if (roomNum == 7) {
            return new Room7();
        }
        else if (roomNum == 8) {
            return new Room8();
        }
        else if (roomNum == 9) {
            return new Room9();
        }
        else if (roomNum == 10) {
            return new Room10();
        }
        return null;
    }
}
