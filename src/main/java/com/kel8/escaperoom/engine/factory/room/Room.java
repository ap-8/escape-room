package com.kel8.escaperoom.engine.factory.room;

import com.linecorp.bot.model.message.Message;

import java.io.IOException;
import java.util.List;

public interface Room {
    String getQuestionAndAnswer() throws IOException;
    String getAnswer() throws IOException;
}
