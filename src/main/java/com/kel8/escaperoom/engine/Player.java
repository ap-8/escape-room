package com.kel8.escaperoom.engine;

import com.kel8.escaperoom.engine.factory.RoomFactory;

import java.io.IOException;

public class Player {
    public String playerId;
    public int healthPoint;
    public int correctAnswer;
    public int roomNum;
    public RoomFactory room;
    public Player(String id){
        playerId = id;
        healthPoint = 3;
        correctAnswer = 0;
        roomNum = 1;
        room = new RoomFactory();
    }

    public String getQuestionAndAnswer() throws IOException {
        return room.getRoom(roomNum).getQuestionAndAnswer();
    }

    public String getAnswerFromRoom() throws IOException {
        return room.getRoom(roomNum).getAnswer();
    }

    public void correct() {
        roomNum++;
        correctAnswer++;
    }

    public void wrong() {
        roomNum++;
        healthPoint--;
    }
}
