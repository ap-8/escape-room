package com.kel8.escaperoom.service;

import com.kel8.escaperoom.engine.Player;

import java.util.ArrayList;
import java.util.List;

public class GameService {
    public static List<Player> playerList = new ArrayList<>();
    public static List<Player> getPlayerList() {
        return playerList;
    }
    public static void addPlayer(Player player) {playerList.add(player);}
    public static boolean checkPlayer(String playerId) {
        for (int i = 0; i < playerList.size(); i++) {
            if (playerList.get(i).playerId.equals(playerId)) {
                return true;
            }
        }
        return false;
    }
    public  static Player getPlayer(String playerId) {
        for (int i = 0; i < playerList.size(); i++) {
            if (playerList.get(i).playerId.equals(playerId)) {
                return playerList.get(i);
            }
        }
        return null;
    }
    public static void removePlayer(String playerId) {
        for (int i = 0; i < playerList.size(); i++) {
            if (playerList.get(i).playerId.equals(playerId)) {
                playerList.remove(i);
                break;
            }
        }
    }
}
