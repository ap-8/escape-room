package com.kel8.escaperoom.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

public class ApiService {
    private static String getApiUrl(int questionRequest) {
        String questionNumber = Integer.toString(questionRequest);
        String apiUrl = "https://ap-gamequestions.herokuapp.com/api/questions/";
        return apiUrl + questionNumber;
    }

    private static JsonNode getJsonResponse(int qNum) throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(getApiUrl(qNum), String.class);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readTree(response.getBody());
    }

    public static String getQuestionAndChoices(int qNum) throws IOException {
        JsonNode jsonResponse = getJsonResponse(qNum);
        JsonNode jsonChoices = jsonResponse.path("choice");
        String question = jsonResponse.path("question").toString() + "\n";
        String choices = "A. " + jsonChoices.get(0) + "\n";
        choices += "B. " + jsonChoices.get(1) + "\n";
        choices += "C. " + jsonChoices.get(2) + "\n";
        choices += "D. " + jsonChoices.get(3);
        return question + choices;
    }

    public static String getAnswer(int qNum) throws IOException {
        JsonNode jsonResponse = getJsonResponse(qNum);
        String answer = jsonResponse.path("answer").toString();
        int value = -1;
        for (int i = 0; i < 4; i++) {
            if (jsonResponse.path("choice").get(i).toString().equals(answer)) {
                value = i;
            }
        }
        if (value == 0) {answer = "A";}
        else if (value == 1) {answer = "B";}
        else if (value == 2) {answer = "C";}
        else if (value == 3) {answer = "D";}
        return answer;
    }
}
