package com.kel8.escaperoom.service;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

@LineMessageHandler
public class ChatService {
    @Autowired
    private LineMessagingClient lineMessagingClient;

    public void sendResponse(String replyToken, List<Message> message) {
        try {
            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, message))
                    .get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Ada error saat ingin membalas chat");
        }
    }

    public void sendResponse(String replyToken, Message message) {
        sendResponse(replyToken, Collections.singletonList(message));
    }

    public void sendStringResponse(String replyToken, String message) {
        sendResponse(replyToken, new TextMessage(message));
    }
}
