package com.kel8.escaperoom.templates;

import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;

public class GameTemplate {
    public static Message startMessage = new TextMessage("Permainan dimulai");
    public static String getStatus(int hp, int roomNum) {
        return "Saat ini anda memiliki nyawa sebanayak: " + hp + "\nAnda telah menyelesaikan "
                +roomNum + " dari 10 ruangan";
    }
}
